import numpy as np
import matplotlib.pyplot as plt

# The dict key is the month and the dict value is the percent rate.
# Month 12 is a year from now.
# Month 360 is 30 years from now
YC = {1: 0.73818, 2: 0.83103, 3: 0.85393, 4: 0.87344,
      5: 0.89156, 6: 0.90619, 9: 0.94035, 12: 0.96712,
      18: 1.01837, 24: 1.05664212, 36: 1.11874651,
      48: 1.17941657, 60: 1.23843929, 72: 1.30179396,
      84: 1.36339275, 96: 1.42007995, 108: 1.47176883,
      120: 1.51815895, 144: 1.60114194, 180: 1.69000072,
      240: 1.78308961, 300: 1.82618131, 360: 1.84928439}

time = [key for key in YC]
time.sort()
rate = [YC[key] for key in time]

# interpolation
interp_rate = np.interp(range(1, 361), time, rate)

PRINCIPAL = 500000  # principal amount
RATE = 3.875 / 100.  # interest rate
TERM = 360  # number of payments (months)

i = RATE / 12.0  # monthly rate
amount = i * PRINCIPAL * pow(1 + i, TERM) / (pow(1 + i, TERM) - 1)

# EXTRA_PAYMENT = amount/12.0

# (a)

val = 0
for ind in range(0, 360):
    # convert the nominal semi-anually-compounded rate to effective monthly rate
    nominal_rate = interp_rate[ind]
    effective_monthly = pow(1 + nominal_rate / 200.0, 1.0 / 6)

    # discount the cash flow
    val += amount / pow(effective_monthly, ind + 1)

print "value of the mortgage is %.2f" % val

# (b)
# with extra payment
EXTRA_PAYMENT = amount / 12.0  # extra amount of principal every month

# recalulate the term that needs to pay


balance = [PRINCIPAL]
principle = []
interest = []
for ind in range(360):
    if balance[-1] < 0:
        break
    interest.append(balance[ind] * i)
    principle.append(amount - interest[ind])
    balance.append(balance[ind] - EXTRA_PAYMENT - principle[ind])

val = 0
for month in range(ind - 1):
    # convert the nominal semi-anually-compounded rate to effective monthly rate
    nominal_rate = interp_rate[month]
    effective_monthly = pow(1 + nominal_rate / 200.0, 1.0 / 6)

    # discount the cash flow
    val += (amount + EXTRA_PAYMENT) / pow(effective_monthly, month + 1)

# discount the last payment seperately

nominal_rate = interp_rate[month + 1]
effective_monthly = pow(1 + nominal_rate / 200.0, 1.0 / 6)
# discount the cash flow

val += (balance[-2] + interest[-1]) / pow(effective_monthly, month + 2)

print "value of the mortgage with extra payment is %.2f" % val





