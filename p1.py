PRINCIPAL = 500000 # principal amount
RATE = 3.875/100. # interest rate
TERM = 360 # number of payments (months)
EXTRA_PAYMENT = 0 # extra amount of principal every month
def print_motgage_schedule(PRINCIPAL, RATE, TERM, EXTRA_PAYMENT):
    
    i = RATE / 12.0  # monthly rate
    
    amount = i * PRINCIPAL * pow(1 + i, TERM) / (pow(1 + i, TERM) - 1) # the amount have to pay every month
    
    #EXTRA_PAYMENT = amount/12.0
    
    
    # calculate the balance
    balance = [PRINCIPAL]
    principle = []
    interest = []
    for ind in range(360):
        if balance[-1] < 0:
            break
        interest.append(balance[ind] * i)
        principle.append(amount + EXTRA_PAYMENT - interest[ind])
        balance.append(balance[ind] -principle[ind])
    
    #keep track of last balance before it went to negative
    last_payment = interest[-1]+ balance[-2]    
    # round the decimal number
        
    interest = ["{:.2f}".format(ele) for ele in interest]
    balance = ["{:.2f}".format(ele) for ele in balance]
    principle = ["{:.2f}".format(ele) for ele in principle]
    
    # print out the schedule of a mortgage
    print "Payment#".rjust(10), "Amount".rjust(10), "Principal".rjust(10), "Interest".rjust(10), "Balance".rjust(20)
    for ind in range(len(interest)-1):
        print repr(ind + 1).rjust(10), "{:.2f}".format(amount).rjust(10), principle[ind].rjust(10), interest[ind].rjust(10), \
        balance[ind + 1].rjust(20)
    
    #handle the last line of schedule
    print repr(ind + 2).rjust(10), "{:.2f}".format(last_payment).rjust(10),balance[-2].rjust(10), interest[ind+1].rjust(10),"0.00".rjust(20)


print_motgage_schedule(PRINCIPAL, RATE, TERM, EXTRA_PAYMENT)


