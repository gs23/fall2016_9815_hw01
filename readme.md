Instruction on how to run the script:

You can run the script by execute *.py file directly in the python shell, this will give you the result.


Example:

\>\>python.exe PATH TO PYFILE/p1.py


There is one print_motgage_schedule function defined in the p1.py, you can call it to by providing with other parameter to print the morgage schedule.

Example:

\>\>print_motgage_schedule(PRINCIPAL, RATE, TERM, EXTRA_PAYMENT)